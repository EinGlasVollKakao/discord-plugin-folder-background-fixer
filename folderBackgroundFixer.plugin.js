/**
 * @name FolderBackgroundFixer
 * @version 0.2.0
 * @description This plugins makes the background color of open server folder the same as closed ones :)
 * @author EinGlasVollKakao, Luramaya
 * 
 */




module.exports = class FolderBackgroundFixer {
    
    load() {}
    
    start() {
        /* we also have to write an additional css rule, that disables the color, when the folders are collapsed
         * when we dont do that, the backgrounds overlap, and that looks ugly
         */
        writeImportantCss();
        
        // the whole thing must be in a different function, cause we have to wait a time in de procedure and we cant do that in the start function :(
        setRightFolderColor();
    }
    
    stop() {
        // get all open folder elements
        let openFolderElements = document.getElementsByClassName("expandedFolderBackground-1kSAf6");
        
        // reset background color
        for (const folder of openFolderElements) {
            folder.style.backgroundColor = "";
        }
    }
    
}

function writeImportantCss(){
    let style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.expandedFolderBackground-1kSAf6.collapsed-uGXEbi {background-color: rgba(0, 0, 0, 0) !important;}';
    document.head.appendChild(style);
}

async function setRightFolderColor() {
    /* we have a problem
     * we can't get the color, if the folders are open
     * so we simply close all open folders
     * sorry open folders, bye :)
    */
    
    let stillOpenFolders = document.getElementsByClassName("expandedFolderIconWrapper-3RwQpD");
    
    for (const folder of stillOpenFolders) {
        folder.click();
        console.log("closed one folder");
    }
    
    // wait 500ms cause we have to
    await new Promise(r => setTimeout(r, 500));
    
    
    // get all (now closed) closed folder elements
    let closedFolderElements = document.getElementsByClassName("folderIconWrapper-1oRIZr");
    
    // variable for all colors
    let colors = [];
    
    // get all colors from the closed folders and put them in colors
    for (const folder of closedFolderElements) {

        let backgroundColor = folder.style.backgroundColor;
        

        
        colors.push(backgroundColor);
    }
    
    console.log(colors);
    
    
    // get all open folder elements
    let openFolderElements = document.getElementsByClassName("expandedFolderBackground-1kSAf6");
    

    // set color 
    let i = 0; // zähler weil i faul
    for (const folder of openFolderElements) {
        folder.style.background = colors[i];
        i++;
    }
    
}
